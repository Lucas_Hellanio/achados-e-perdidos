/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.lpoo.banco;

import br.com.lpoo.basicas.Achados;
import br.com.lpoo.basicas.Perdidos;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lukas
 */
public class BancoDeDados implements Serializable {

    Connection conn;

    public void openConnection() throws ClassNotFoundException, SQLException {
        try {

            Class.forName("oracle.jdbc.OracleDriver");
            if (conn == null || conn.isClosed()) {
                conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "Bancodedados1");
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(BancoDeDados.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeConnection() {
        if (conn != null) {
            try {
                conn.close();
                conn = null;
            } catch (SQLException ex) {
                Logger.getLogger(BancoDeDados.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void inserirDadosPerdidos(Perdidos p) throws SQLException, ClassNotFoundException {

        openConnection();
        try {

            String query = "INSERT INTO PERDIDOS (NOMEOBJ,DESCRICAO,TIPO,EMAIL,CONTATO) VALUES (?,?,?,?,?)";

            PreparedStatement pstm;
            pstm = conn.prepareStatement(query);
            pstm.setString(1, p.getNomeObj());
            pstm.setString(2, p.getDescricaoObj());
            pstm.setString(3, p.getTipoObje());
            pstm.setString(4, p.getEmail());
            pstm.setString(5, p.getContato());
            ResultSet rs = pstm.executeQuery();

            rs.close();
            pstm.close();

        } catch (Exception ex) {
            throw ex;

        } finally {

            closeConnection();
        }

    }

    public void inserirDadosAchados(Achados a) throws ClassNotFoundException, SQLException {
        openConnection();
        try {

            String query = "INSERT INTO ACHADOS (NOMEOBJ,DESCRICAO,TIPO) VALUES (?,?,?)";

            PreparedStatement pstm;
            pstm = conn.prepareStatement(query);
            pstm.setString(1, a.getNomeObj());
            pstm.setString(2, a.getDescricaoObj());
            pstm.setString(3, a.getTipoObje());

            ResultSet rs = pstm.executeQuery();

            rs.close();
            pstm.close();

        } catch (Exception ex) {
            throw ex;

        } finally {

            closeConnection();
        }

    }

    public List<Achados> buscarAchados() throws ClassNotFoundException, SQLException {
        openConnection();
        List<Achados> achados = new ArrayList();
        Achados objeto;

        try {

            String query = "SELECT * FROM ACHADOS";
            PreparedStatement pstm = conn.prepareStatement(query);

            ResultSet rs = pstm.executeQuery();

            while (rs.next()) {

                objeto = new Achados();

                objeto.setNomeObj(rs.getString("NOMEOBJ"));
                objeto.setDescricaoObj(rs.getString("DESCRICAO"));
                objeto.setTipoObje(rs.getString("TIPO"));

                achados.add(objeto);

            }

            rs.close();
            pstm.close();

        } catch (Exception e) {
            throw e;

        } finally {

            closeConnection();
        }

        return achados;

    }

}
