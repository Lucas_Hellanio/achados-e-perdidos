package br.com.lpoo.basicas;

import java.io.Serializable;

public class Perdidos extends Objeto implements Serializable {
    
    private String email;
    private String contato;
    
    public boolean validarDados(Perdidos p) {
        
        boolean r = false;
        if (p.getNomeObj().equals("") || p.getDescricaoObj().equals("") || p.getTipoObje().equals("")
                || p.getContato().equals("") || p.getEmail().equals("")) {
            
            r = false;
        } else {
            
            r = true;
        }
        return r;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getContato() {
        return contato;
    }
    
    public void setContato(String contato) {
        this.contato = contato;
    }
    
}
