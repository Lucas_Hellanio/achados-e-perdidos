package br.com.lpoo.basicas;

import java.io.Serializable;

public class Achados extends Objeto implements Serializable {

    public boolean validarDados(Achados a) {

        boolean r=false;
        if (a.getNomeObj().equals("") || a.getDescricaoObj().equals("") || a.getTipoObje().equals("")) {

            r=false;
        } else {

            r= true;
        }
        return r;
    }

}
