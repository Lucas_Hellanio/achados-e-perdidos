package br.com.lpoo.basicas;

import java.io.Serializable;

public class Objeto implements Serializable {

   
   
    private String nomeObj;
    private String descricaoObj;
    private String tipoObje;
   

  
    public String getNomeObj() {
        return nomeObj;
    }

    public void setNomeObj(String nomeObj) {
        this.nomeObj = nomeObj;
    }

    public String getDescricaoObj() {
        return descricaoObj;
    }

    public void setDescricaoObj(String descricaoObj) {
        this.descricaoObj = descricaoObj;
    }
  /**
     * @return the tipoObje
     */
    public String getTipoObje() {
        return tipoObje;
    }

    /**
     * @param tipoObje the tipoObje to set
     */
    public void setTipoObje(String tipoObje) {
        this.tipoObje = tipoObje;
    }


    
}
