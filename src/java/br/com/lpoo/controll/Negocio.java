/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.lpoo.controll;

import br.com.lpoo.banco.BancoDeDados;
import br.com.lpoo.basicas.Achados;
import br.com.lpoo.basicas.Perdidos;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 *
 * @author lukas
 */
@ManagedBean
public class Negocio implements Serializable {

    private BancoDeDados banco;
    private List<Achados> achados;
    private List<Perdidos> perdidos;
    private List<String> tipoObj;
    private Achados objAch;
    private Perdidos objPerd;
    private boolean exibirPainel = false;

    public Negocio() throws ClassNotFoundException, SQLException {

        banco = new BancoDeDados();
        achados = new ArrayList();
        perdidos = new ArrayList();
        // tipoObj = new ArrayList();
        objPerd = new Perdidos();
        objAch = new Achados();

        listarAchados();

    }

    public void listarAchados() throws ClassNotFoundException, SQLException {

        try {
            setAchados(getBanco().buscarAchados());
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage("Erro ao tentar acessar o banco!"));

        }
    }

    public void inserirPerdido() throws SQLException, ClassNotFoundException {

        try {
            boolean verificar = objPerd.validarDados(objPerd);
            if (verificar == true) {
                getBanco().inserirDadosPerdidos(getObjPerd());
                objPerd = new Perdidos();
                FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage("Salvo com sucesso!"));
            } else {

                FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage("ERRO! Todos os campos são obrigatórios!"));

            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage("Erro ao tentar acessar o banco!"));

        }
    }

    public void inserirAchado() throws SQLException, ClassNotFoundException {

        try {
            boolean verificar = objAch.validarDados(objAch);
            if (verificar == true) {
                banco.inserirDadosAchados(objAch);
                objAch = new Achados();
                FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage("Salvo com sucesso!"));
            } else {
                FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage("ERRO! Todos os campos são obrigatórios!"));

            }

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("growl", new FacesMessage("Erro ao tentar acessar o banco!"));
        }

        listarAchados();
    }

    public void exibirPainel() {

        exibirPainel = true;
    }

    /**
     * @return the banco
     */
    public BancoDeDados getBanco() {
        return banco;
    }

    /**
     * @param banco the banco to set
     */
    public void setBanco(BancoDeDados banco) {
        this.banco = banco;
    }

    /**
     * @return the achados
     */
    public List<Achados> getAchados() {
        return achados;
    }

    /**
     * @param achados the achados to set
     */
    public void setAchados(List<Achados> achados) {
        this.achados = achados;
    }

    /**
     * @return the perdidos
     */
    public List<Perdidos> getPerdidos() {
        return perdidos;
    }

    /**
     * @param perdidos the perdidos to set
     */
    public void setPerdidos(List<Perdidos> perdidos) {
        this.perdidos = perdidos;
    }

    /**
     * @return the tipoObj
     */
    public List<String> getTipoObj() {
        return tipoObj;
    }

    /**
     * @param tipoObj the tipoObj to set
     */
    public void setTipoObj(List<String> tipoObj) {
        this.tipoObj = tipoObj;
    }

    /**
     * @return the objAch
     */
    public Achados getObjAch() {
        return objAch;
    }

    /**
     * @param objAch the objAch to set
     */
    public void setObjAch(Achados objAch) {
        this.objAch = objAch;
    }

    /**
     * @return the objPerd
     */
    public Perdidos getObjPerd() {
        return objPerd;
    }

    /**
     * @param objPerd the objPerd to set
     */
    public void setObjPerd(Perdidos objPerd) {
        this.objPerd = objPerd;
    }

    /**
     * @return the exibirPainel
     */
    public boolean isExibirPainel() {
        return exibirPainel;
    }

    /**
     * @param exibirPainel the exibirPainel to set
     */
    public void setExibirPainel(boolean exibirPainel) {
        this.exibirPainel = exibirPainel;
    }

}
